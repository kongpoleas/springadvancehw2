package com.example.jpahomework1v2;

import com.example.jpahomework1v2.repository.ArticleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class JpaHomework1v2Application {

    public static void main(String[] args) {

        SpringApplication.run(JpaHomework1v2Application.class, args);
    }
//    @Bean
//    public CommandLineRunner run(ArticleRepository articleRepository){
//        return (args -> {
//            System.out.println(articleRepository.findAll());
//        });
//    }
}

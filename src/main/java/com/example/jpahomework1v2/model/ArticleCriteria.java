package com.example.jpahomework1v2.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArticleCriteria {
    private String cate_name;
    private String user_name;
}

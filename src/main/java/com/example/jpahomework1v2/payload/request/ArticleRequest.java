package com.example.jpahomework1v2.payload.request;

import lombok.*;


@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ArticleRequest {
    private String title;
    private String description;
    private Long user_id;
    private  Long cate_id;
}

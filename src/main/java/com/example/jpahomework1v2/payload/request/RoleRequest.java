package com.example.jpahomework1v2.payload.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleRequest {
    private String name;
}

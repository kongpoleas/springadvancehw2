package com.example.jpahomework1v2.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class UserRequest {
    private Long Role_id;
    private String username;
    private String password;
}

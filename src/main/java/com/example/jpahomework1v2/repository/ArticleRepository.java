package com.example.jpahomework1v2.repository;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.model.ArticleCriteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> , JpaSpecificationExecutor<Article>, PagingAndSortingRepository<Article, Long> {

    List<Article> findByTitle(String title);
    Article findArticleById(Long id);

    @Query(value = "SELECT *FROM article a INNER JOIN users u on u.id = a.user_id where u.id = :id", nativeQuery = true)
    List<Article> getArticleByUserId(Long id);

    @Query(value = "SELECT *FROM article a INNER JOIN categories c on c.id = a.category_id where c.id = :id", nativeQuery = true)
    List<Article> getArticleByCate_id(Long id);

}

package com.example.jpahomework1v2.repository;

import com.example.jpahomework1v2.model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Categories, Long>, PagingAndSortingRepository<Categories, Long> {
   public Categories findCategoriesById(Long id);
}


package com.example.jpahomework1v2.repository;

import com.example.jpahomework1v2.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolesRepository extends JpaRepository<Roles, Long>, PagingAndSortingRepository<Roles, Long> {
    List<Roles> findByName(String naem);
    Roles findRolesById(Long id);
}

package com.example.jpahomework1v2.repository;

import com.example.jpahomework1v2.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<Users, Long>, PagingAndSortingRepository<Users, Long> {
    Users findUsersById(Long id);

    @Query(value = "SELECT *FROM users u INNER JOIN user_roles1 r on u.id = r.user_id\n" +
            "INNER JOIN roles r2 on r2.id = r.role_id\n" +
            "where lower(r2.name) like lower(concat('%', :role_name,'%'))", nativeQuery = true)
    List<Users> findUsersByRole(@Param("role_name") String role_name);
}

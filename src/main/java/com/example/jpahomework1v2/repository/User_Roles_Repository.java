package com.example.jpahomework1v2.repository;

import com.example.jpahomework1v2.model.User_Roles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface User_Roles_Repository extends JpaRepository<User_Roles, Long> {
}

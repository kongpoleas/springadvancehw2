package com.example.jpahomework1v2.rest;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.model.ArticleCriteria;
import com.example.jpahomework1v2.payload.request.ArticleRequest;
import com.example.jpahomework1v2.repository.ArticleRepository;
import com.example.jpahomework1v2.service.ArticleService;
import com.example.jpahomework1v2.specification.ArticleSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/article/api/v1")
public class ArticleRestController {
    @Autowired
    private ArticleService articleService;

    @Autowired
    ArticleRepository articleRepository;

    @GetMapping("/findAll/{pageNo}/{pageSize}")
    public List<Article> findAllArticle(@PathVariable int pageNo, @PathVariable int pageSize){
        return articleService.findAll(pageNo, pageSize);
    }

    @PostMapping("/postArticle")
    public Article postAricle(@RequestBody ArticleRequest articleRequest){
        return articleService.postArticle(articleRequest);
    }

    @PutMapping("/updateArticle/{article_id}")
    public Article updateArticle(@PathVariable Long article_id, @RequestBody ArticleRequest articleRequest){
        return articleService.updateArticle(article_id, articleRequest);
    }

    @DeleteMapping("/deleteArticle/{article_id}")
    public Boolean deleteArticle(@PathVariable Long article_id){
        articleService.deleteArticle(article_id);
        return true;
    }

    @GetMapping("/getArticleByUserId/{user_id}")
    public List<Article> getArticleByUserId(@PathVariable Long user_id){
            return articleService.getArticleByUserId(user_id);
    }

    @GetMapping("/getArticleByCate_id/{cate_id}")
    public List<Article> getArticleByCate_id(@PathVariable Long cate_id){
        return articleService.getArtcleByCate_id(cate_id);
    }

    @GetMapping("/findArticleByUser_NameAndCategory_Name")
    public List<Article> findArticleByUser_NameAndCategory_Name
            (@RequestParam ("cate_name") String  cate_name,
             @RequestParam ("user_name") String user_name){
        ArticleCriteria articleCriteria = new ArticleCriteria();
        articleCriteria.setUser_name(user_name);
        articleCriteria.setCate_name(cate_name);

        ArticleSpecification articleSpecification = new ArticleSpecification(articleCriteria);
        return articleRepository.findAll(articleSpecification);

    }
}

package com.example.jpahomework1v2.rest;

import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.payload.request.CategoryRequest;
import com.example.jpahomework1v2.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category/api/v1")
public class CategoryRestController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/findAll/{pageNo}/{pageSize}")
    public List<Categories> findAllCategory(@PathVariable int pageNo, @PathVariable int pageSize){
        return categoryService.findAllCategory(pageNo, pageSize);
    }

    @PostMapping("/postCategory")
    public Categories postCategory(@RequestBody CategoryRequest categoryRequest){
        return categoryService.postCategory(categoryRequest);
    }

    @PutMapping("/updateCategory/{category_id}")
    public Categories updateCategory(@PathVariable Long category_id, CategoryRequest categoryRequest){
        return categoryService.updateCategory(category_id, categoryRequest);
    }

    @DeleteMapping("/deleteCategory/{category_id}")
    public Boolean deleteCategory(@PathVariable Long category_id){
        categoryService.deleteCategory(category_id);
        return true;
    }
}

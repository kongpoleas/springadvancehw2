package com.example.jpahomework1v2.rest;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.model.Roles;
import com.example.jpahomework1v2.payload.request.CategoryRequest;
import com.example.jpahomework1v2.payload.request.RoleRequest;
import com.example.jpahomework1v2.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role/api/v1")
public class RoleRestController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/findAll/{pageNo}/{pageSize}")
    public List<Roles> findAllArticle(@PathVariable int pageNo, @PathVariable int pageSize){
        return roleService.findAll(pageNo, pageSize);
    }

    @PostMapping("/postRoles")
    public Roles postRoles(@RequestBody RoleRequest request){
        return roleService.postRoles(request);
    }

    @PutMapping("/updateRoles/{role_id}")
    public Roles updateRole(@PathVariable Long role_id, RoleRequest request){
        return roleService.updateRoles(role_id, request);
    }

    @DeleteMapping("/deleteRoles/{role_id}")
    public Boolean deletRoles(@PathVariable Long role_id){
        roleService.deleteRoles(role_id);
        return true;
    }
}

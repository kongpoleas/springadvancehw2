package com.example.jpahomework1v2.rest;


import com.example.jpahomework1v2.model.Users;
import com.example.jpahomework1v2.payload.request.UserRequest;
import com.example.jpahomework1v2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/api/v1")
public class UserRestController {

    @Autowired
    private UserService service;


    @GetMapping("/findAll/{pageNo}/{pageSize}")
    public List<Users> findAllUsers(@PathVariable int pageNo, @PathVariable int pageSize){
        return service.findAllUser(pageNo, pageSize);
    }

    @PostMapping("/postUser")
    public Users postUser(@RequestBody UserRequest userRequest){
        return service.postUser(userRequest);
    }

    @PutMapping("/updateUser/{user_id}")
    public Users updateUser(@PathVariable Long user_id, UserRequest userRequest){
         return service.update(user_id, userRequest);
    }

    @DeleteMapping("/deleteUser/{user_id}")
    public Boolean deleteUser(@PathVariable long user_id){
      service.deleteUser(user_id);
      return true;
    }

    @GetMapping("/getUserbySpecificRole")
    public List<Users> getUserbySpecificRole(@RequestParam("role_name") String role_name){
        return service.findUserByRole(role_name);
    }

}

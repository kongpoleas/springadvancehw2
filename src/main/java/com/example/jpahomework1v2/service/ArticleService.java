package com.example.jpahomework1v2.service;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.payload.request.ArticleRequest;

import java.util.List;

public interface ArticleService{
    List<Article> findAll(int pageNo, int pageSize);
    Article postArticle(ArticleRequest articleRequest);
    Article updateArticle(Long id, ArticleRequest articleRequest);
    void deleteArticle(Long id);
    List<Article> getArticleByUserId(Long id);
    List<Article> getArtcleByCate_id(Long id);

}

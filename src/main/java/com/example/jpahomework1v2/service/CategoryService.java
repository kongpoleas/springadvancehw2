package com.example.jpahomework1v2.service;

import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.payload.request.CategoryRequest;

import java.util.List;

public interface CategoryService {

    List<Categories> findAllCategory(int pageNo, int pageSize);
    Categories postCategory(CategoryRequest categoryRequest);
    Categories updateCategory(Long id, CategoryRequest categoryRequest);
    void deleteCategory(Long id);
}

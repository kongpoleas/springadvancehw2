package com.example.jpahomework1v2.service;

import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.model.Roles;
import com.example.jpahomework1v2.payload.request.CategoryRequest;
import com.example.jpahomework1v2.payload.request.RoleRequest;

import java.util.List;

public interface RoleService {
    List<Roles> findAll(int pageNo, int pageSize);
    Roles postRoles(RoleRequest roleRequest);
    Roles updateRoles(Long id, RoleRequest roleRequest);
    void deleteRoles(Long id);
}

package com.example.jpahomework1v2.service;

import com.example.jpahomework1v2.model.Users;
import com.example.jpahomework1v2.payload.request.UserRequest;

import java.util.List;

public interface UserService {
    List<Users> findAllUser(int pageNo, int pageSize);
    Users postUser(UserRequest userRequest);
    Users update(Long id,UserRequest userRequest);
    void deleteUser(Long id);
    List<Users> findUserByRole(String role_name);
}

package com.example.jpahomework1v2.service.imp;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.model.Users;
import com.example.jpahomework1v2.payload.request.ArticleRequest;
import com.example.jpahomework1v2.repository.ArticleRepository;
import com.example.jpahomework1v2.repository.CategoryRepository;
import com.example.jpahomework1v2.repository.UserRepository;
import com.example.jpahomework1v2.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImp implements ArticleService {

    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Article> findAll(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Article> pageResult =  articleRepository.findAll(pageable);
        return pageResult.toList();
    }

    @Override
    public Article postArticle(ArticleRequest articleRequest) {
        Users users = userRepository.findUsersById(articleRequest.getUser_id());
        Categories categories = categoryRepository.findCategoriesById(articleRequest.getCate_id());
        Article article = new Article();
//        article.setId(articleRequest.getId());
        article.setTitle(articleRequest.getTitle());
        article.setDescription(articleRequest.getDescription());
        article.setCategories(categories);
        article.setUsers(users);
        return articleRepository.save(article);
    }

    @Override
    public Article updateArticle(Long id, ArticleRequest articleRequest) {
        Users users = userRepository.findUsersById(articleRequest.getUser_id());
        Categories categories = categoryRepository.findCategoriesById(articleRequest.getCate_id());
        Article article = articleRepository.findArticleById(id);
//        Article article1 = new Article();
        article.setId(article.getId());
        article.setTitle(articleRequest.getTitle());
        article.setDescription(articleRequest.getDescription());
        article.setUsers(users);
        article.setCategories(categories);
        return articleRepository.save(article);
    }

    @Override
    public void deleteArticle(Long id) {
         articleRepository.deleteById(id);
    }

    @Override
    public List<Article> getArticleByUserId(Long id) {
        return articleRepository.getArticleByUserId(id);
    }

    @Override
    public List<Article> getArtcleByCate_id(Long id) {
        return articleRepository.getArticleByCate_id(id);
    }


}

package com.example.jpahomework1v2.service.imp;

import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.model.Users;
import com.example.jpahomework1v2.payload.request.CategoryRequest;
import com.example.jpahomework1v2.repository.CategoryRepository;
import com.example.jpahomework1v2.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    @Autowired
    private  CategoryRepository categoryRepository;

    @Override
    public List<Categories> findAllCategory(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Categories> pageResult =  categoryRepository.findAll(pageable);
        return pageResult.toList();
    }

    @Override
    public Categories postCategory(CategoryRequest categoryRequest) {
        Categories categories = new Categories();
        categories.setName(categoryRequest.getName());
        return categoryRepository.save(categories);
    }

    @Override
    public Categories updateCategory(Long id, CategoryRequest categoryRequest) {
        Categories categories = categoryRepository.findCategoriesById(id);
        categories.setName(categoryRequest.getName());
        return categoryRepository.save(categories);
    }

    @Override
    public void deleteCategory(Long id) {
        categoryRepository.deleteById(id);
    }
}

package com.example.jpahomework1v2.service.imp;

import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.model.Roles;
import com.example.jpahomework1v2.payload.request.RoleRequest;
import com.example.jpahomework1v2.repository.RolesRepository;
import com.example.jpahomework1v2.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolesServiceImp implements RoleService {

    @Autowired
    private RolesRepository repository;

    @Override
    public List<Roles> findAll(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Roles> pageResult =  repository.findAll(pageable);
        return pageResult.toList();
    }

    @Override
    public Roles postRoles(RoleRequest roleRequest) {
        Roles roles = new Roles();
        roles.setName(roleRequest.getName());
        return repository.save(roles);
    }

    @Override
    public Roles updateRoles(Long id, RoleRequest roleRequest) {
        Roles roles = repository.findRolesById(id);
        roles.setName(roleRequest.getName());
        return repository.save(roles);
    }

    @Override
    public void deleteRoles(Long id) {
        repository.deleteById(id);
    }
}

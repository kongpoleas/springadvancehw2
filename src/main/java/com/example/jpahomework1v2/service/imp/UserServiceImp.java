package com.example.jpahomework1v2.service.imp;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.model.Roles;
import com.example.jpahomework1v2.model.User_Roles;
import com.example.jpahomework1v2.model.Users;
import com.example.jpahomework1v2.payload.request.UserRequest;
import com.example.jpahomework1v2.repository.RolesRepository;
import com.example.jpahomework1v2.repository.UserRepository;
import com.example.jpahomework1v2.repository.User_Roles_Repository;
import com.example.jpahomework1v2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RolesRepository repository;

    @Autowired
    private User_Roles_Repository user_roles_repository;

    @Override
    public List<Users> findAllUser(int pageNo, int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<Users> pageResult =  userRepository.findAll(pageable);
        return pageResult.toList();
    }

    @Override
    public Users postUser(UserRequest userRequest) {

        Roles  roles = repository.findRolesById(userRequest.getRole_id());
        Users users1 = new Users();
        users1.setUsername(userRequest.getUsername());
        users1.setPassword(userRequest.getPassword());

        User_Roles user_roles = new User_Roles();
        user_roles.setUsers(users1);
        user_roles.setRoles(roles);
        user_roles_repository.save(user_roles);
        return userRepository.save(users1);
    }

    @Override
    public Users update(Long id, UserRequest userRequest) {
        Roles  roles = repository.findRolesById(userRequest.getRole_id());
        Users users = userRepository.findUsersById(id);

        users.setUsername(userRequest.getUsername());
        users.setPassword(userRequest.getPassword());

        User_Roles user_roles = new User_Roles();
        user_roles.setUsers(users);
        user_roles.setRoles(roles);
        user_roles_repository.save(user_roles);
        return userRepository.save(users);
    }

    @Override
    public void deleteUser(Long id) {
        Users users = userRepository.findUsersById(id);
        userRepository.delete(users);

    }

    @Override
    public List<Users> findUserByRole(String role_name) {
        return userRepository.findUsersByRole(role_name);
    }


}

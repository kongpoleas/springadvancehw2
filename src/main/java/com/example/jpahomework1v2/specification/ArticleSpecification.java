package com.example.jpahomework1v2.specification;

import com.example.jpahomework1v2.model.Article;
import com.example.jpahomework1v2.model.ArticleCriteria;
import com.example.jpahomework1v2.model.Categories;
import com.example.jpahomework1v2.model.Users;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class ArticleSpecification implements Specification<Article> {
    private ArticleCriteria criteria;

    @Override
    public Predicate toPredicate(
            Root<Article> root,
            CriteriaQuery<?> query,
            CriteriaBuilder cb) {

        List<Predicate> predicates = new ArrayList<>();
        if(criteria.getUser_name() == null || criteria.getCate_name()  == null)
            return null;
        else {
            Join<Article, Categories> join = root.join("categories", JoinType.INNER);
            predicates.add(cb.like(cb.lower(join.get("name")),
                    "%" + criteria.getCate_name() + "%"));

            Join<Article, Users> join1 = root.join("users", JoinType.INNER);
            predicates.add(cb.like(cb.lower(join1.get("username")),
                    "%" + criteria.getUser_name() + "%"));
        }
        Predicate[] pre = new Predicate[predicates.size()];
        return cb.and(predicates.toArray(pre));
    }
}
